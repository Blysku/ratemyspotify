<%--
  Created by IntelliJ IDEA.
  User: blysku
  Date: 17/10/18
  Time: 18:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Rate My Spotify</title>
    <jsp:include page="headlinks.jsp"/>
</head>
<body>
<jsp:include page="header.jsp"/>
<div class="albumList mx-auto row justify-content-center">
    <c:forEach items="${albumList}" var="album">
        <div class="albumElement col-xl-2 col-lg-3 col-md-4 col-sm-5 list-group d-inline-flex justify-content-center">
            <img src="${album.images.get(0).url}" class="h-100 w-100">
            <h1 class="albumName">${album.name}</h1>
        </div>
    </c:forEach>
</div>
<jsp:include page="footer.jsp"/>
<jsp:include page="srcjs.jsp"/>
</body>
</html>
