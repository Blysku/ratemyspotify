<%--
  Created by IntelliJ IDEA.
  User: Bartosz
  Date: 15/10/2018
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Register</title>
    <jsp:include page="headlinks.jsp"/>
</head>
<body>
<jsp:include page="header.jsp"/>

<form class="navbar-dark" action="/register" method="post">
    <div class = "row">
        <div class = "col">
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" name="username"  placeholder="username">
            </div>
        </div>
        <div class = "col">
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-secondary">Zarejestruj</button>
</form>

<jsp:include page="footer.jsp"/>
<jsp:include page="srcjs.jsp"/>
</body>
</html>
