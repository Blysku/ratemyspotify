package pl.kkbk.ratemyspotify.models;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity @Getter @Setter
public class Album {

    @Id
    @JsonProperty("id")
    String id;

    @JsonProperty("name")
    String name;

    @ManyToMany @Getter
    List<Artist> artistList;
}
