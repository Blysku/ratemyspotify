package pl.kkbk.ratemyspotify.models;

import lombok.Getter;

import javax.persistence.*;
import java.util.List;

@Entity
public class Artist {

    @Id
    String id;

    @Getter
    String name;

    @ManyToMany
    List<Album> albumList;
}
