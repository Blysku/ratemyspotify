package pl.kkbk.ratemyspotify.utilities;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.kkbk.ratemyspotify.services.SchedulerService;


@Component
public class TaskScheduler {

    private final SchedulerService schedulerService;

    public TaskScheduler(SchedulerService schedulerService) {
        this.schedulerService = schedulerService;
    }

    @Scheduled(initialDelay = 0, fixedDelay = 55 * 60 * 1000)
    public void refreshToken(){

        schedulerService.updateAccessToken();

    }

}
