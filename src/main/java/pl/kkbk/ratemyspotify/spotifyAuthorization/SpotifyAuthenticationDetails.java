package pl.kkbk.ratemyspotify.spotifyAuthorization;


public class SpotifyAuthenticationDetails {


    public static final String GRANT_TYPE_KEY = "grant_type";
    public static final String GRANT_TYPE_VALUE = "authorization_code";
    public static final String AUTH_CODE_KEY = "code";
    public static final String REDIRECT_URI_KEY = "redirect_uri";
    public static final String REQUEST_URI = "https://accounts.spotify.com/api/token";
    public static final String HEADER_AUTHORIZATION_KEY = "Authorization";
    public static final String HEADER_AUTHORIZATION_VALUE_PREFIX = "Basic ";
    public static final String DELIMITER = ":";
//    public static final String CLIENT_ID = "86624497662643859235d19db20302b5";
    public static final String CLIENT_ID = "e223ed8a2def490991d5cc17baf61c87";
//    public static final String CLIENT_SECRET = "934621b079324101b66cee83979cc13e";
    public static final String CLIENT_SECRET = "12f5b644cbbd43e29361987cf35850fc";
    public static final String REDIRECT_URL = "http://localhost:9000/authentication/handle";
    public static final String AUTHORIZATION_REQUEST = "https://accounts.spotify.com/authorize/?client_id="
            + CLIENT_ID
            + "&response_type=code&redirect_uri="
            + REDIRECT_URL
            + "&scope=user-read-private%20user-read-email";

}
