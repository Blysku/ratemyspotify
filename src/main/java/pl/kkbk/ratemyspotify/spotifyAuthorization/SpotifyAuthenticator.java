package pl.kkbk.ratemyspotify.spotifyAuthorization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;

import static pl.kkbk.ratemyspotify.spotifyAuthorization.SpotifyAuthenticationDetails.*;

@Component
public class SpotifyAuthenticator {

    private final RestTemplate restTemplate;

    @Autowired
    public SpotifyAuthenticator(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public SpotifyAuthenticationTokenDetails getTokenDetails(MultiValueMap<String, String> requestBody){

        HttpHeaders requestHeaders = getHttpHeaders();

        HttpEntity<?> entity = new HttpEntity<>(requestBody, requestHeaders);

        ResponseEntity<SpotifyAuthenticationTokenDetails> responseEntity =
                restTemplate.exchange(
                        REQUEST_URI,
                        HttpMethod.POST,
                        entity,
                        SpotifyAuthenticationTokenDetails.class);

        return responseEntity.getBody();
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        requestHeaders.setAccessControlRequestMethod(HttpMethod.POST);
        String encodedClient = Base64.getEncoder().encodeToString((CLIENT_ID + DELIMITER + CLIENT_SECRET).getBytes());

        requestHeaders.set(HEADER_AUTHORIZATION_KEY,
                HEADER_AUTHORIZATION_VALUE_PREFIX + encodedClient);
        return requestHeaders;
    }


}
