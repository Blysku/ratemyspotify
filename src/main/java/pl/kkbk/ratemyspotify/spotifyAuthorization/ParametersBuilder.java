package pl.kkbk.ratemyspotify.spotifyAuthorization;

import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static pl.kkbk.ratemyspotify.spotifyAuthorization.SpotifyAuthenticationDetails.*;

@Component
public class ParametersBuilder {

    public MultiValueMap<String, String> getParametersToAuthentication(String code) {
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add(GRANT_TYPE_KEY, GRANT_TYPE_VALUE);
        requestBody.add(AUTH_CODE_KEY, code);
        requestBody.add(REDIRECT_URI_KEY, REDIRECT_URL);
        return requestBody;
    }

    public MultiValueMap<String, String> getParametersToGetRefreshToken(String refreshToken) {
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add(GRANT_TYPE_KEY, "refresh_token");
        requestBody.add("refresh_token", refreshToken);
        return requestBody;
    }

}
