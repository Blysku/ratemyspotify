package pl.kkbk.ratemyspotify.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kkbk.ratemyspotify.repositories.UserRepository;
import pl.kkbk.ratemyspotify.spotifyAuthorization.ParametersBuilder;
import pl.kkbk.ratemyspotify.spotifyAuthorization.SpotifyAuthenticationTokenDetails;
import pl.kkbk.ratemyspotify.spotifyAuthorization.SpotifyAuthenticator;

import java.net.URISyntaxException;

@Service
public class SchedulerService {

    private final ParametersBuilder parametersBuilder;
    private final SpotifyAuthenticator spotifyAuthenticator;
    private final UserRepository repository;

    @Autowired
    public SchedulerService(ParametersBuilder parametersBuilder, SpotifyAuthenticator spotifyAuthenticator, UserRepository repository) {
        this.parametersBuilder = parametersBuilder;
        this.spotifyAuthenticator = spotifyAuthenticator;
        this.repository = repository;
    }

    private String refreshToken(String refreshToken) {

        SpotifyAuthenticationTokenDetails spotifyAuthenticationTokenDetails =
                spotifyAuthenticator.getTokenDetails(parametersBuilder.getParametersToGetRefreshToken(refreshToken));

        return spotifyAuthenticationTokenDetails.getAccessToken();
    }

    public void updateAccessToken() {

        repository.findAll().
                forEach(user -> {
                    user.setAccessToken(refreshToken(user.getRefreshToken()));
                    repository.save(user);
                });
    }
}
