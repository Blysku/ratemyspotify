package pl.kkbk.ratemyspotify.services;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class SpotifyAlbumDetails {

    @JsonProperty("name")
    private String name;

    @JsonProperty("type")
    private String type;

    private String id;

    private List<ImageListWrapper> images;

    private List<ArtistWrapper> artists;
}
