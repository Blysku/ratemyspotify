package pl.kkbk.ratemyspotify.services;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AlbumWrapper {

    private List<SpotifyAlbumDetails> items;

}
