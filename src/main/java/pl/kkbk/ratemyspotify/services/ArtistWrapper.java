package pl.kkbk.ratemyspotify.services;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ArtistWrapper {

    private String name;
}
