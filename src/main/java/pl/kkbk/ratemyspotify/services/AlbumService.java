package pl.kkbk.ratemyspotify.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.kkbk.ratemyspotify.models.Album;

import java.util.List;

@Service
public class AlbumService {

    private SpotifyAPIAccessor spotifyAPIAccessor;

    @Autowired
    public AlbumService(SpotifyAPIAccessor spotifyAPIAccessor) {
        this.spotifyAPIAccessor = spotifyAPIAccessor;
    }

    public List<SpotifyAlbumDetails> findNewRealeasedAlbums(String accessToken){
        return spotifyAPIAccessor.getNewReleases(accessToken);
    }

    public Album getAlbum(String accessToken, String id){

        return spotifyAPIAccessor.getAlbum(accessToken,id);
    }


}
