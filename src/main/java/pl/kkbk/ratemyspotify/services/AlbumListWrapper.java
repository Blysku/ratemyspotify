package pl.kkbk.ratemyspotify.services;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Getter @Setter
public class AlbumListWrapper {


    private AlbumWrapper albums;

}
