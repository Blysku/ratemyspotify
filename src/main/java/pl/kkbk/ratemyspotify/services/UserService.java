package pl.kkbk.ratemyspotify.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.kkbk.ratemyspotify.models.User;
import pl.kkbk.ratemyspotify.repositories.UserRepository;
import pl.kkbk.ratemyspotify.spotifyAuthorization.ParametersBuilder;
import pl.kkbk.ratemyspotify.spotifyAuthorization.SpotifyAuthenticationTokenDetails;
import pl.kkbk.ratemyspotify.spotifyAuthorization.SpotifyAuthenticator;

@Service
public class UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository repository;
    private final SpotifyAuthenticator spotifyAuthenticator;
    private final ParametersBuilder parametersBuilder;

    @Autowired
    public UserService(PasswordEncoder passwordEncoder, UserRepository repository, SpotifyAuthenticator spotifyAuthenticator, ParametersBuilder parametersBuilder) {
        this.passwordEncoder = passwordEncoder;
        this.repository = repository;
        this.spotifyAuthenticator = spotifyAuthenticator;
        this.parametersBuilder = parametersBuilder;
    }

    public User attachTokens(User user, String code) {


        SpotifyAuthenticationTokenDetails tokenDetails = spotifyAuthenticator.getTokenDetails(parametersBuilder.getParametersToAuthentication(code));
        User newUser = new User(user.getUsername(), passwordEncoder.encode(user.getPassword()), tokenDetails.getAccessToken(), tokenDetails.getRefreshToken());
        return repository.save(newUser);
    }
}
