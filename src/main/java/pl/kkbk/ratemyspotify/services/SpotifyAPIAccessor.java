package pl.kkbk.ratemyspotify.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.kkbk.ratemyspotify.models.Album;
import pl.kkbk.ratemyspotify.spotifyAuthorization.SpotifyAuthenticationTokenDetails;

import java.util.List;

import static pl.kkbk.ratemyspotify.spotifyAuthorization.SpotifyAuthenticationDetails.HEADER_AUTHORIZATION_KEY;
import static pl.kkbk.ratemyspotify.spotifyAuthorization.SpotifyAuthenticationDetails.HEADER_AUTHORIZATION_VALUE_PREFIX;
import static pl.kkbk.ratemyspotify.spotifyAuthorization.SpotifyAuthenticationDetails.REQUEST_URI;

@Component
public class SpotifyAPIAccessor {

    private final RestTemplate restTemplate;


    @Autowired
    public SpotifyAPIAccessor(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<SpotifyAlbumDetails> getNewReleases(String accessToken){

        HttpHeaders requestHeaders = getHttpHeaders(accessToken);

        HttpEntity<?> entity = new HttpEntity<>(requestHeaders);

        //ParameterizedTypeReference<List<SpotifyAlbumDetails>> responseType = new ParameterizedTypeReference<List<SpotifyAlbumDetails>>(){};

        ResponseEntity<AlbumListWrapper> responseEntity =
                restTemplate.exchange(
                        "https://api.spotify.com/v1/browse/new-releases",
                        HttpMethod.GET,
                        entity,
                        AlbumListWrapper.class);

        return responseEntity.getBody().getAlbums().getItems();
    }

    public Album getAlbum(String accessToken, String id){

        HttpHeaders requestHeaders = getHttpHeaders(accessToken);

        HttpEntity<?> entity = new HttpEntity<>(requestHeaders);

        ResponseEntity<Album> responseEntity =
                restTemplate.exchange(
                        "https://api.spotify.com/v1/albums/" + id,
                        HttpMethod.GET,
                        entity,
                        Album.class);


        return responseEntity.getBody();
    }


    private HttpHeaders getHttpHeaders(String accessToken){
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAccessControlRequestMethod(HttpMethod.GET);

        requestHeaders.set("Authorization",
                "Bearer " + accessToken);
        return requestHeaders;
    }
}
