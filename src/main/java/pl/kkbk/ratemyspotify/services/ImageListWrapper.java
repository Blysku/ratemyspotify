package pl.kkbk.ratemyspotify.services;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ImageListWrapper {

    private String url;

}
