package pl.kkbk.ratemyspotify.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/home2")
    public String getHomeJsp() {
        return "home";
    }
}
