package pl.kkbk.ratemyspotify.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.kkbk.ratemyspotify.models.User;
import pl.kkbk.ratemyspotify.services.UserService;

import javax.servlet.http.HttpSession;
import java.net.URISyntaxException;

@Controller
public class SpotifyAuthenticationController {

    private final UserService service;

    @Autowired
    public SpotifyAuthenticationController(UserService service) {
        this.service = service;
    }

    @GetMapping("/authentication/handle")
    public String exchangeCodeForToken(
            @RequestParam(required = false) String code,
            @RequestParam(required = false) String error,
            HttpSession session) throws URISyntaxException {

        if (error != null){
            session.invalidate();
            return null;
        }

        service.attachTokens((User) session.getAttribute("user"), code);
        session.invalidate();

        return "redirect:/home";
    }
}
