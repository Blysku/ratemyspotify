package pl.kkbk.ratemyspotify.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.kkbk.ratemyspotify.models.User;
import pl.kkbk.ratemyspotify.services.AlbumService;

import java.security.Principal;

@Controller
public class AlbumController {

    private AlbumService albumService;

    @Autowired
    public AlbumController(AlbumService albumService) {
        this.albumService = albumService;
    }

    @GetMapping("/home")
    public String getNewReleaseAlbum(Model model, Principal principal){

        String accessToken = ((User)((UsernamePasswordAuthenticationToken) principal).getPrincipal()).getAccessToken();

        model.addAttribute("albumList", albumService.findNewRealeasedAlbums(accessToken));

        return "home";
    }
}
