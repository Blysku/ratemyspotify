package pl.kkbk.ratemyspotify.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.kkbk.ratemyspotify.models.User;
import pl.kkbk.ratemyspotify.spotifyAuthorization.SpotifyAuthenticationDetails;

import javax.servlet.http.HttpSession;

@Controller
public class AuthenticationController {

    @GetMapping("/register")
    public String registerJSP(){
        return "register";
    }

    @GetMapping("/authentication")
    public String authorization(){

        return "redirect:" + SpotifyAuthenticationDetails.AUTHORIZATION_REQUEST;
    }

    @PostMapping("/register")
    public String register(@ModelAttribute User user, HttpSession session){

        session.setAttribute("user", user);

        return "redirect:/authentication";
    }


}
