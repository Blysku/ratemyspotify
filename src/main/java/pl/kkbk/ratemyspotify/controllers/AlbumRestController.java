package pl.kkbk.ratemyspotify.controllers;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.kkbk.ratemyspotify.models.Album;
import pl.kkbk.ratemyspotify.models.User;
import pl.kkbk.ratemyspotify.services.AlbumService;
import pl.kkbk.ratemyspotify.services.SpotifyAlbumDetails;

import java.security.Principal;
import java.util.List;

@RestController
public class AlbumRestController {

    private AlbumService albumService;

    public AlbumRestController(AlbumService albumService) {
        this.albumService = albumService;
    }

    @GetMapping("/start")
    public List<SpotifyAlbumDetails> releasedAlbums(Principal principal){

        String accessToken = ((User)((UsernamePasswordAuthenticationToken) principal).getPrincipal()).getAccessToken();

        return albumService.findNewRealeasedAlbums(accessToken);
    }

    @GetMapping("/search/{id}")
    public Album getAlbum(@PathVariable(name = "id") String id, Principal principal){

        String accessToken = ((User)((UsernamePasswordAuthenticationToken) principal).getPrincipal()).getAccessToken();

        return albumService.getAlbum(accessToken,id);

    }

}
