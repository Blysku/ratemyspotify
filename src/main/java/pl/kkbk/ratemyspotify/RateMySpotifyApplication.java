package pl.kkbk.ratemyspotify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class RateMySpotifyApplication {

    public static void main(String[] args) {
        SpringApplication.run(RateMySpotifyApplication.class, args);
    }
}
